import random
import re
import os.path
from input import input_name,input_username,input_password,input_logging

def create_account(name,username,password):
    with open(f'{username}-info.txt','a+') as f:
        f.write(f'{username} | {name} | {password}\n')

def create_business():
    pass

def login(username,password,is_logging_in,logged_in):
    if is_logging_in == 'YES'.lower():
        with open(f'{username}-info.txt','r+') as f:
            s = f.read()
            if os.path.isfile(f'{username}-info.txt'):
                input_password = input('Put in your password: ')
                for file_ in s:
                    if re.search(input_password,s):
                        print("You are now logged in")
                        logged_in = True
                    else:
                        print("Sorry, we couldn't find a user matching those credentials")



create_account(input_name,input_username,input_password)
login(input_username,input_password,input_logging,False)